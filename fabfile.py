# -*- coding:utf-8 -*-
import datetime

from fabric.api import env, sudo, cd
from fabric.decorators import task

try:
    from fabfile_config_local import PASSWORD
except ImportError:
    pass
else:
    env.password = PASSWORD

#env.user = 'ubuntu'
DEFAULT_HOST = '52.6.72.82'
env.user = 'ubuntu'
env.key_filename = '~/.ssh/GTG_05032016.pem'

env.roledefs = {
    'all': [DEFAULT_HOST, ],
    'default': [DEFAULT_HOST],
}

CONFIG = {
    DEFAULT_HOST: {
        'sources_folder': '/home/django/projects/shineconvrkit',
        'interpreter': '/home/django/.virtualenvs/shineconvrkit/bin/python',
        # 'celery_tasks': 'celery-mystatsonline',
    },
}
"""
sudo su - django
cd /home/django/projects/shineconvrkit
. /home/django/.virtualenvs/shineconvrkit/bin/activate
"""

@task
def deploy():
    env.settings = CONFIG[env['host']]
    print(45, env.settings)
    with cd(env.settings['sources_folder']):
        sudo('git pull', user='django')
        # sudo('pip install -r requirements/production.txt', user='django')
        sudo('%(interpreter)s manage.py migrate --noinput --settings=config.settings.production' % env.settings, user='django')
        sudo('%(interpreter)s manage.py collectstatic --noinput ' % env.settings, user='django')

        sudo('touch uwsgi.ini', user='django')

        print(datetime.datetime.now())
from fabric.api import runs_once, lcd, local, task

@task
@runs_once
def register_deployment(git_path):
    with(lcd(git_path)):
        revision = local('git log -n 1 --pretty="format:%H"', capture=True)
        branch = local('git rev-parse --abbrev-ref HEAD', capture=True)
        local('curl https://intake.opbeat.com/api/v1/organizations/c5674ae31fbc41529aadeb197db5fc85/apps/d67b672a3a/releases/'
              ' -H "Authorization: Bearer 74a82295c93dcedeb04db5d0e0ed34204dc8fd7e"'
              ' -d rev="{}"'
              ' -d branch="{}"'
              ' -d status=completed'.format(revision, branch))