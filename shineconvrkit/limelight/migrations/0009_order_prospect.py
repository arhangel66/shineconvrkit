# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-01-06 02:59
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('limelight', '0008_prospect_task_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='prospect',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='limelight.Prospect'),
        ),
    ]
