import re
from pprint import pprint

from django.conf import settings
from django.contrib.auth.mixins import AccessMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import Template, Context, RequestContext
from django.template.loader import render_to_string
from django.views.generic import TemplateView, FormView, View
from django.contrib import messages
from shineconvrkit.limelight.api import LimeLightApi, get_client_ip
from shineconvrkit.limelight.card_type import get_card_type
from shineconvrkit.limelight.models import CrmLogs, Prospect, Order
from django.http import HttpResponse
from shineconvrkit.limelight.tasks import call
from shineconvrkit.main.models import SnippetGlobal, SnippetPage

shineconvrkit_id = 235


class OnlyMobileMixin(AccessMixin):
    """
    if it is not mobile will redirect to back page
    """
    back = 'home'
    def dispatch(self, request, *args, **kwargs):
        if request.flavour != 'mobile':
            return HttpResponseRedirect(reverse(self.back))

        return super().dispatch(request, *args, **kwargs)


class OnlyFullMixin(AccessMixin):
    """
    if it is not mobile will redirect to back page
    """
    mobile = 'payment'
    def dispatch(self, request, *args, **kwargs):
        if request.flavour == 'mobile':
            return HttpResponseRedirect(reverse(self.mobile))

        return super().dispatch(request, *args, **kwargs)

class HasOrderMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.
    """
    back = 'home'

    def dispatch(self, request, *args, **kwargs):
        if not (request.session.get('main_order_id') or request.session.get('order_task')):
            return HttpResponseRedirect(reverse(self.back))

        return super().dispatch(request, *args, **kwargs)


class HasProductMixin(object):
    def dispatch(self, request, *args, **kwargs):
        print(61, request.session.get('main_product_id'))
        if not (request.session.get('main_product_id')):
            return HttpResponseRedirect(reverse(self.back))

        return super().dispatch(request, *args, **kwargs)



class HasProspectMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.
    """
    back = 'home'

    def dispatch(self, request, *args, **kwargs):
        if not (request.session.get('prospect_task')):
            return HttpResponseRedirect(reverse(self.back))

        return super().dispatch(request, *args, **kwargs)


class AbstractBaseView(object):
    def is_celery_error(self, celery_task_id, request=None):
        if request.session.get(celery_task_id) is None:
            return False
        log_id = call.AsyncResult(request.session[celery_task_id]).get()
        log = CrmLogs.objects.get(id=log_id)
        if log.is_error:
            # messages.add_message(request, messages.INFO, log.get_error_text())
            if request:
                messages.add_message(request, messages.INFO, log.get_error_text())
            return True
        return log


class MyTemplateView(TemplateView, AbstractBaseView):

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        # context['request'] = request
        context['test'] = 'test222'
        # messages.add_message(request, messages.INFO, 'test41')

        html = render_to_string(self.get_template_names()[0], context,
                                context_instance=RequestContext(request))

        print(74, request.flavour)

        html = self.process_snippets(html, page=request.path)
        html = self.change_static(html, flavour=request.flavour)
        html = self.set_csrf_token(html)
        # print(23, html)


        t = Template(html)

        html = t.render(RequestContext(request, context))
        # print(27, html)
        return HttpResponse(html)

    def change_static(self, html, flavour=None):
        path = '/static/v1/'
        if flavour == 'mobile':
            path = '/static/mobile/v1/'

        html = re.sub(r'(src="|href=")([^"]+\.html"|js|img|css|images|fonts)', '\\1%s\\2' % path, html)
        html = re.sub(r'(openNewWindow\(\&\#39\;)([^&]+\.html\&\#39\;)', '\\1/%s\\2' % path, html)
        html = re.sub(r'(<input[^>]+data-qty=\")([^"]+)(\")', '\\1\\2" value="\\2\\3', html) # add val to select inputs
        html = html.replace('//static', '/static')
        return html

    def set_csrf_token(self, html):
        html = re.sub(r'(<form[^>]+>)', '\\1 \n {% csrf_token %}', html)
        return html

    def process_snippets(self, html, page):
        """
        Add all snippets to html page.
        Returns new updated html page.
        """
        dic = {}
        for s in SnippetGlobal.objects.all():
            dic[s.name] = s

        for s in SnippetPage.objects.filter(page=page):
            dic[s.name] = s

        for name, s in dic.items():
            html = s.add_snippet(html)

        return html

class SendProspectMixin(object):
    def post(self, request, *args, **kwargs):
        api = LimeLightApi()
        prospect_data = {}
        for key in request.POST.keys():
            prospect_data[key] = request.POST[key]
        prospect_data['ipAddress'] = get_client_ip(request)
        prospect_data['address1'] = prospect_data.get('shippingAddress1') if 'shippingAddress1' in prospect_data else ''
        prospect_data['country'] = prospect_data.get('shippingCountry') if 'shippingCountry' in prospect_data else ''
        prospect_data['state'] = prospect_data.get('shippingState') if 'shippingState' in prospect_data else ''
        prospect_data['city'] = prospect_data.get('shippingCity') if 'shippingCity' in prospect_data else ''
        prospect_data['zip'] = prospect_data.get('shippingZip') if 'shippingZip' in prospect_data else ''

        request.session['prospect_task'] = api.send_prospect(prospect_data).id
        print(146, request.session['prospect_task'])
        # create prospect in DB
        # prospect_dict_for_db = Prospect().prepare_dict_from_ll(prospect_data)
        # prospect_dict_for_db['task_id'] = request.session['prospect_task']
        # prospect = Prospect.objects.create(**prospect_dict_for_db)
        if not '/' in self.next:
            return HttpResponseRedirect(reverse(self.next))
        return HttpResponseRedirect(self.next)


class SendOrderWithProspectMixin(object):
    product = ''
    next = '/'

    def post(self, request, *args, **kwargs):
        log = self.is_celery_error('prospect_task', request)
        if log is True:
            return HttpResponseRedirect(reverse(self.back))
        log.save_prospect()

        if self.request.session.get('main_product_id'):
            self.product = self.request.session.get('main_product_id')

        prospect_id = log.get_prospect_id()
        request.session['prospect_id'] = prospect_id

        # Prospect.objects.filter(task_id=request.session['prospect_task']).update(limelight_id=prospect_id)
        order_data = {}
        for key in request.POST.keys():
            order_data[key] = request.POST[key]

        if not order_data.get('creditCardType'):
            order_data['creditCardType'] = get_card_type(order_data.get('creditCardNumber', None))
        order_data['expirationDate'] = "%s%s" % (order_data.get('expmonth'), order_data.get('expyear'))
        order_data['tranType'] = 'Sale'

        product = settings.PRODUCTS.get(self.product)
        if product and 'id' in product:
            order_data['productId'] = product['id']
            if 'price' in product:
                order_data['dynamic_product_price_' + str(product['id'])] = product['price']

        api = LimeLightApi()

        request.session['order_task'] = api.send_order_with_prospect(order_data, prospect_id).id
        request.session['order'] = [{'product': product, 'total': product['price'] * int(product['qty'])}]
        if not '/' in self.next:
            return HttpResponseRedirect(reverse(self.next))
        return HttpResponseRedirect(self.next)



class SendOrderMixin(object):
    product = ''
    next = '/'

    def post(self, request, *args, **kwargs):

        # Prospect.objects.filter(task_id=request.session['prospect_task']).update(limelight_id=prospect_id)
        order_data = {}
        for key in request.POST.keys():
            order_data[key] = request.POST[key]
        order_data['expirationDate'] = "%s%s" % (order_data.get('expmonth'), order_data.get('expyear'))
        order_data['tranType'] = 'Sale'

        pprint(order_data)

        product = settings.PRODUCTS.get(order_data['packSelect'])
        print(184, product)
        if product and 'id' in product:
            order_data['productId'] = product['id']
            if 'price' in product:
                order_data['dynamic_product_price_' + str(product['id'])] = product['price']
            if 'qty' in product and product['qty'] != 1:
                order_data['product_qty_%s' % product['id']] = product['qty']

        order_data['ipAddress'] = get_client_ip(request)
        order_data['creditCardType'] = get_card_type(order_data.get('creditCardNumber', None))
        api = LimeLightApi()

        request.session['order_task'] = api.send_order_from_dict(order_data, ip=get_client_ip(request)).id
        request.session['order'] = [{'product': product, 'total': product['price'] * int(product['qty'])}]

        return HttpResponseRedirect(self.next)


class SendUpsellMixin(object):
    actions = {}
    next = '/'

    def get(self, request, *args, **kwargs):
        key = request.GET.get('action')
        if key:
            product_key = self.actions.get(key)
            if product_key is None:
                return HttpResponseRedirect(self.next)
            product = settings.PRODUCTS.get(product_key)

            # messages.add_message(request, messages.INFO, 'test132')
            log = self.is_celery_error('order_task', request)
            if log is True:
                return HttpResponseRedirect(reverse(self.back))

            request.session['main_order_id'] = log.get_order_id()

            api = LimeLightApi()
            api.new_order_card_on_file_async(request.session['main_order_id'], product['id'], price=product['price'],
                                             prospect_id=request.session.get('prospect_id'), qty=product['qty'])
            request.session['order'] += [{'product': product, 'total': product['price'] * int(product['qty'])}]

            return HttpResponseRedirect(self.next)
        return super().get(request, *args, **kwargs)


class HomeView(MyTemplateView, SendProspectMixin):
    next = '/checkout/'

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class CheckoutView(OnlyFullMixin, MyTemplateView, SendOrderMixin):
    next = '/upsell/'
    product = 'shineconvrkit'
    back = 'home'
    mobile = 'payment'

class UpsellView(HasOrderMixin, SendUpsellMixin, MyTemplateView):
    next = '/upsell2/'
    actions = {'yes1': 'upsell1_1',
               'yes3': 'upsell1_3',
               'yes5': 'upsell1_5'}
    back = 'checkout'


class Upsell2View(HasOrderMixin, SendUpsellMixin, MyTemplateView):
    next = '/success/'
    actions = {'yes1': 'upsell2_1',
               'yes3': 'upsell2_3',
               'yes5': 'upsell2_5'}

    back = 'checkout'


class SuccessView(HasOrderMixin, MyTemplateView):
    back = 'checkout'

    def clean_session(self):
        list_keys = ['main_order_id', 'prospect_task', 'order_task']
        for key in list_keys:
            if key in self.request.session:
                self.request.session.pop(key)

    def get(self, request, *args, **kwargs):
        log = self.is_celery_error('order_task', request)
        if log is True:
            return HttpResponseRedirect(reverse(self.back))
        print(201)
        if not request.session.get('main_order_id'):
            print(202, log, log.get_order_id())
            request.session['main_order_id'] = log.get_order_id()

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total'] = self.get_total(self.request.session.get('order', []))
        context['subtotal'] = context['total'] - self.request.session.get('order', [])[0]['total']
        context['order'] = self.request.session.get('order')
        context['order_id'] = self.request.session['main_order_id']
        context['address'] = Order.objects.get(limelight_id=self.request.session['main_order_id']).get_address_dict()
        # self.request.session
        self.clean_session()
        return context

    def get_total(self, order):
        total = 0
        for line in order:
            if line and type(line) == type({}) and line.get('total'):
                total += float(line.get('total'))
        return round(total, 2)


class SelectView(OnlyMobileMixin, MyTemplateView):
    back = 'home'
    next = 'shipping'

    def post(self, request, *args, **kwargs):
        if 'packSelect'in request.POST:
            request.session['main_product_id'] = request.POST['packSelect']
            product = settings.PRODUCTS.get(request.session['main_product_id'])
            product['total'] = product['price'] * product['qty']
            request.session['main_product'] = product
            return HttpResponseRedirect(reverse(self.next))


class ShippingView(OnlyMobileMixin, HasProductMixin, MyTemplateView, SendProspectMixin):
    back = 'select'
    next = 'payment'


class PaymentView(OnlyMobileMixin, HasProspectMixin, SendOrderWithProspectMixin, MyTemplateView):
    back = 'shipping'
    next = 'upsell'