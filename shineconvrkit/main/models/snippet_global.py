from django.db import models
from django.contrib import admin
from .snippet_base import SnippetBase


class SnippetGlobalManager(models.Manager):
    def create_snippet(self, name, value, type):
        s = self.model(name=name, value=value, type=type)
        s.save()
        return s


class SnippetGlobal(SnippetBase):
    objects = SnippetGlobalManager()

    class Meta:
        verbose_name = 'snippet: global'
        verbose_name_plural = 'snippets: global'
        ordering = ['name']

    def __str__(self):
        return self.name


@admin.register(SnippetGlobal)
class SnippetGlobalAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'value')
    list_editable = ('value', 'type')
